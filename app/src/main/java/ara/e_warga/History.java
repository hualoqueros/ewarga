package ara.e_warga;

public class History {
    private int id;
    private String jenis_surat;
    private String tanggal_surat;
    private int status_surat;


    //Constructor

    public History(int id, String jenis_surat, String tanggal_surat, int status_surat) {
        this.id = id;
        this.jenis_surat = jenis_surat;
        this.tanggal_surat = tanggal_surat;
        this.status_surat = status_surat;
    }

    //Setter, getter

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getJenis_surat() { return jenis_surat; }

    public void setJenis_surat(String jenis_surat) {
        this.jenis_surat = jenis_surat;
    }

    public String getTanggal_surat() {
        return tanggal_surat;
    }

    public void setTanggal_surat(String tanggal_surat) {
        this.tanggal_surat = tanggal_surat;
    }

    public int getStatus_surat() {
        return status_surat;
    }

    public void setStatus_surat(int status_surat) {
        this.status_surat = status_surat;
    }
}
