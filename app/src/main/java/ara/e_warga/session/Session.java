package ara.e_warga.session;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.util.ArrayList;

import ara.e_warga.api.model.User;

public class Session {
    static final String IS_LOGIN = "is_login";
    static final String USER = "user";
    static final String PHOTO_UPLOAD_PRODUCT = "photo_uplaod_product";

    static SharedPreferences getSharedPreferences(Context context){
        return PreferenceManager.getDefaultSharedPreferences(context);
    }

    public static void setIsLogin(Context context){
        SharedPreferences.Editor editor = getSharedPreferences(context).edit();
        editor.putBoolean(IS_LOGIN,true);
        editor.commit();
    }

    public static boolean isLogin(Context context){
        return getSharedPreferences(context).getBoolean(IS_LOGIN, false);
    }

    public static void setUser(Context context, User user){
        Gson gson = new Gson();
        String userData = gson.toJson(user);
        SharedPreferences.Editor editor = getSharedPreferences(context).edit();
        editor.putString(USER,userData);
        editor.commit();
    }

    public static User getUser(Context context){
        Gson gson = new Gson();
        String jsonText = getSharedPreferences(context).getString(USER, null);
        User user = gson.fromJson(jsonText, new TypeToken<User>(){}.getType());
        return user;
    }

    public static void destroy(Context context){
        SharedPreferences.Editor editor = getSharedPreferences(context).edit();
        editor.remove(USER);
        editor.remove(IS_LOGIN);
        editor.commit();
    }

    public static void setUploadProduct(Context context, ArrayList<String> allPath){
        Gson gson = new Gson();
        String paths = gson.toJson(allPath);
        SharedPreferences.Editor editor = getSharedPreferences(context).edit();
        editor.putString(PHOTO_UPLOAD_PRODUCT,paths);
        editor.commit();
    }

    public static ArrayList<String> getUploadProduct(Context context){
        Gson gson = new Gson();
        String jsonText = getSharedPreferences(context).getString(PHOTO_UPLOAD_PRODUCT, null);
        ArrayList<String> text = gson.fromJson(jsonText, new TypeToken<ArrayList<String>>(){}.getType());
        return text;
    }
}
