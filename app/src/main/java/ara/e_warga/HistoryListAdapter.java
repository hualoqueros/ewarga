package ara.e_warga;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

public class HistoryListAdapter extends BaseAdapter {

    private Context mContext;
    private List<History> mHistoryList;

    //Constructor


    public HistoryListAdapter(Context mContext, List<History> mHistory) {
        this.mContext = mContext;
        this.mHistoryList = mHistory;
    }

    @Override
    public int getCount() {
        return mHistoryList.size();
    }

    @Override
    public Object getItem(int position) {
        return mHistoryList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View v = View.inflate(mContext, R.layout.item_history_list, null);
        TextView tvJenisSurat = (TextView)v.findViewById(R.id.tv_jenis_surat);
        TextView tvTanggalSurat = (TextView)v.findViewById(R.id.tv_tanggal_surat);
        ImageView ivStatus = (ImageView)v.findViewById(R.id.iv_status);

        //Set text for text view
        tvJenisSurat.setText(mHistoryList.get(position).getJenis_surat());
        tvTanggalSurat.setText(mHistoryList.get(position).getTanggal_surat());
        ivStatus.setImageResource(statusIcon(mHistoryList.get(position).getStatus_surat())); //set icon (id status)

        //Save History id to tag
        v.setTag(mHistoryList.get(position).getId());

        return v;
    }

    private int statusIcon(int status){
        int resource = 0;
        if (status == 1) {
            resource = R.drawable.ic_checked;
        }else if (status == 2){
            resource = R.drawable.ic_fail;
        }else if (status == 3) {
            resource = R.drawable.ic_ongoing;
        }
        return resource;
    }
}
