package ara.e_warga;

import android.app.Activity;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

public class HistoryActivity extends AppCompatActivity{
    private ListView lvHistory;
    private HistoryListAdapter adapter;
    private List<History> mHistoryList;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_history);

        lvHistory = (ListView) findViewById(R.id.listview_history);

        mHistoryList = new ArrayList<>();
        //Add sample data for list
        //Gat data from DB, webservice here
        mHistoryList.add(new History(1, "Keterangan Tinggal", "17 Agustus 2018", 1));
        mHistoryList.add(new History(2, "Buat Akte Kelahiran", "8 Juli 2018", 2));
        mHistoryList.add(new History(3, "Numpang Nikah", "22 Mei 2018", 1));
        mHistoryList.add(new History(4, "SKCK", "10 Maret 2018", 3));
        mHistoryList.add(new History(5, "Keterangan Tinggal", "17 Agustus 2018", 1));
        mHistoryList.add(new History(6, "Buat Akte Kelahiran", "8 Juli 2018", 2));
        mHistoryList.add(new History(7, "Numpang Nikah", "22 Mei 2018", 1));
        mHistoryList.add(new History(8, "SKCK", "10 Maret 2018", 3));
        mHistoryList.add(new History(9, "Keterangan Tinggal", "17 Agustus 2018", 1));
        mHistoryList.add(new History(10, "Buat Akte Kelahiran", "8 Juli 2018", 2));
        mHistoryList.add(new History(11, "Numpang Nikah", "22 Mei 2018", 1));
        mHistoryList.add(new History(12, "SKCK", "10 Maret 2018", 3));

        //Init adapter
        adapter = new HistoryListAdapter(getApplicationContext(), mHistoryList);
        lvHistory.setAdapter(adapter);

        lvHistory.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                //ke halaman detail surat
                Toast.makeText(getApplicationContext(), "Clicked history id = "+ view.getTag(), Toast.LENGTH_SHORT).show();
            }
        });

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case android.R.id.home:
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
