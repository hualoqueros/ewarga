package ara.e_warga;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.Spinner;

import ara.e_warga.fragment.DFragmentPopUpCameraOrGallery;
import ara.e_warga.fragment.EmptyUploadFragment;
import ara.e_warga.fragment.FilledUploadFragment;
import butterknife.BindView;

public class FormActivity extends AppCompatActivity implements View.OnClickListener {
    private static final int TAMBAH_GAMBAR_REQUEST = 1;

    @BindView(R.id.wrap_upload)
    FrameLayout wrapUpload;
    private FragmentManager mFragmentManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_form);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        Window window = getWindow();
        window.setStatusBarColor(getResources().getColor(R.color.colorPrimaryDark));
        mFragmentManager = getSupportFragmentManager();
        init();

    }

    public void init(){
        Spinner spinner = (Spinner) findViewById(R.id.spinner_surat);
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this,
                R.array.surat, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);

        Button submitBtn = (Button) findViewById(R.id.submit);
        submitBtn.setOnClickListener(this);
        loadUploadEmptyFragment();
    }

    @Override
    public void onBackPressed() {
        finish();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case android.R.id.home:
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View view) {
        if (view.getId()==R.id.submit){
            startActivity(new Intent(FormActivity.this, ThankYouActivity.class));
            finish();
        }else if(view.getId()==R.id.wrap_upload){

        }
    }

    private void loadUploadEmptyFragment()
    {
        Fragment initialFragment = new EmptyUploadFragment();
        FragmentTransaction fragmentTransaction = mFragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.wrap_upload, initialFragment);
        fragmentTransaction.commit();
    }

    private void loadFilledUploadFragment()
    {
        Fragment initialFragment = new FilledUploadFragment();
        FragmentManager fm = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fm.beginTransaction();
        fragmentTransaction.replace(R.id.wrap_upload, initialFragment);
        fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit();
    }

    public void change(Fragment whichFragment) {
        if (whichFragment instanceof EmptyUploadFragment){
            loadFilledUploadFragment();
        }
//        else if (whichFragment instanceof EmptyMaterialFragment){
//            EmptyMaterialFragment emptyMaterialFragment = (EmptyMaterialFragment) whichFragment;
//            checkedList = emptyMaterialFragment.getMaterials();
//            loadMaterialFilledFragment( emptyMaterialFragment.getMaterials() );
//        }else if (whichFragment instanceof FilledMaterialFragment){
//            FilledMaterialFragment filledMaterialFragment = (FilledMaterialFragment) whichFragment;
//            checkedList = filledMaterialFragment.getMaterials();
//            reloadMaterialFilledFragment( filledMaterialFragment );
//        }
    }
}
