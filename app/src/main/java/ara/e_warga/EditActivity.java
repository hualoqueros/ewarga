package ara.e_warga;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.TargetApi;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.LoaderManager;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.app.LoaderManager.LoaderCallbacks;

import android.content.CursorLoader;
import android.content.Loader;
import android.database.Cursor;
import android.net.Uri;
import android.os.AsyncTask;

import android.os.Build;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.EditorInfo;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import ara.e_warga.api.BaseApi;
import ara.e_warga.api.model.User;
import ara.e_warga.api.request.UserInterface;
import ara.e_warga.session.Session;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.Manifest.permission.READ_CONTACTS;

public class EditActivity extends AppCompatActivity implements DatePickerDialog.OnDateSetListener {

    // UI references.
    private AutoCompleteTextView mEmailView;
    private EditText mPasswordView;
    private EditText mNik;
    private EditText mFirstName;
    private EditText mLastName;
    private EditText mBirthDate;
    private View mProgressView;
    private View mLoginFormView;
    private int day,month,year;
    private int day_x,month_x,year_x;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit);

        mBirthDate = (EditText) findViewById(R.id.birthdate);

        mBirthDate.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                Calendar c = Calendar.getInstance();
                year = c.get(Calendar.YEAR);
                month = c.get(Calendar.MONTH);
                day = c.get(Calendar.DAY_OF_MONTH);
                DatePickerDialog datePickerDialog = new DatePickerDialog(EditActivity.this,
                        AlertDialog.THEME_HOLO_LIGHT, EditActivity.this,year,month,day);
                datePickerDialog.show();
            }
        });

        Spinner spinnerRw = (Spinner) findViewById(R.id.rwList);
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this,
                R.array.RW, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerRw.setAdapter(adapter);

        Spinner spinnerRt = (Spinner) findViewById(R.id.rtList);
        ArrayAdapter<CharSequence> adapterRt = ArrayAdapter.createFromResource(this,
                R.array.RT, android.R.layout.simple_spinner_item);
        adapterRt.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerRt.setAdapter(adapterRt);

        Spinner spinnerRelig = (Spinner) findViewById(R.id.religList);
        ArrayAdapter<CharSequence> adapterRelig = ArrayAdapter.createFromResource(this,
                R.array.Agama, android.R.layout.simple_spinner_item);
        adapterRelig.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerRelig.setAdapter(adapterRelig);

        Spinner spinnerNation = (Spinner) findViewById(R.id.nationList);
        ArrayAdapter<CharSequence> adapterNation = ArrayAdapter.createFromResource(this,
                R.array.Kewarganegaraan, android.R.layout.simple_spinner_item);
        adapterNation.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerNation.setAdapter(adapterNation);

        Spinner spinnerEdu = (Spinner) findViewById(R.id.eduList);
        ArrayAdapter<CharSequence> adapterEdu = ArrayAdapter.createFromResource(this,
                R.array.Pendidikan, android.R.layout.simple_spinner_item);
        adapterEdu.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerEdu.setAdapter(adapterEdu);

        Spinner spinnerGender = (Spinner) findViewById(R.id.genderList);
        ArrayAdapter<CharSequence> adapterGender = ArrayAdapter.createFromResource(this,
                R.array.JenisKelamin, android.R.layout.simple_spinner_item);
        adapterGender.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerGender.setAdapter(adapterGender);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
    }

    @Override
    public void onDateSet(DatePicker datePicker, int i, int i1, int i2) {
        year_x = i;
        month_x = i1+1;
        day_x = i2;
        Calendar c = Calendar.getInstance();
        if (year_x > c.get(Calendar.YEAR)){
            Toast.makeText(EditActivity.this, "Year is invalid", Toast.LENGTH_SHORT).show();
            mBirthDate.setText("");
            return;
        }else if (year_x == c.get(Calendar.YEAR) && month_x > c.get(Calendar.MONTH)+1) {
            Toast.makeText(EditActivity.this, "Month is invalid", Toast.LENGTH_SHORT).show();
            mBirthDate.setText("");
            return;
        }else if(month_x == c.get(Calendar.MONTH)+1 && day_x > c.get(Calendar.DAY_OF_MONTH)){
            Toast.makeText(EditActivity.this, "Day of month is invalid", Toast.LENGTH_SHORT).show();
            mBirthDate.setText("");
            return;
        }
        mBirthDate.setText(day_x+"/"+month_x+"/"+year_x);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case android.R.id.home:
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
