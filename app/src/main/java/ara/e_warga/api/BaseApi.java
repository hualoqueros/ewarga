package ara.e_warga.api;

import java.util.concurrent.TimeUnit;

import ara.e_warga.BuildConfig;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class BaseApi {
    private static Retrofit retrofitClient;
    private static String BASE_URL = BuildConfig.API_BASE_URL;

    public static Retrofit getClient(){

        if( retrofitClient == null ){
            okhttp3.logging.HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
            interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);

            OkHttpClient okClient = new OkHttpClient.Builder()
                    .readTimeout(60, TimeUnit.SECONDS)
                    .connectTimeout(120,TimeUnit.SECONDS)
                    .addInterceptor(interceptor)
                    .build();

            retrofitClient = new Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .client(okClient)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
        }
        return retrofitClient;
    }
}
