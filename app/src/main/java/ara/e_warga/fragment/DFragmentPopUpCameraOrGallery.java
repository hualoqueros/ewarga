package ara.e_warga.fragment;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.StrictMode;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.DialogFragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

import ara.e_warga.R;
import ara.e_warga.session.Session;
import butterknife.BindView;
import butterknife.ButterKnife;

public class DFragmentPopUpCameraOrGallery extends DialogFragment {
    @BindView(R.id.from_gallery)
    TextView fromGallery;
    @BindView(R.id.from_camera)
    TextView fromCamera;
    Uri fileUri;
    Uri tempUri;


    private static final int MY_PERMISSION_ACCESS_CAMERA = 1000;
    private static final int MY_PERMISSION_ACCESS_CAWRITE_EXTERNAL_STORAGE = 1001;
    private static final int CAMERA_CAPTURE_IMAGE_REQUEST_CODE = 100;
    private static final int GET_FROM_GALLERY = 101;
    public static final int MEDIA_TYPE_IMAGE = 1;
    private static final int MEDIA_TYPE_VIDEO = 2;
    private static final String IMAGE_DIRECTORY_NAME = "ewarga";

    String mPermissionCamera = Manifest.permission.CAMERA;
    String mPermissionWriteExternalStorage = Manifest.permission.WRITE_EXTERNAL_STORAGE;
    public static boolean hasPermissions(Context context, String... permissions) {
        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && context != null && permissions != null) {
            for (String permission : permissions) {
                if (ActivityCompat.checkSelfPermission(context, permission) != PackageManager.PERMISSION_GRANTED) {
                    return false;
                }
            }
        }
        return true;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_popup_get_picture, container,
                false);
        ButterKnife.bind(this, rootView);
        String[] PERMISSIONS = {mPermissionCamera,mPermissionWriteExternalStorage};
        if (!hasPermissions(getActivity().getApplicationContext(),PERMISSIONS)) {
            Log.v("samudra","tidak allowed");
            ActivityCompat.requestPermissions(getActivity(),PERMISSIONS,MY_PERMISSION_ACCESS_CAMERA);
        }

        fromGallery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getFromGallery();
            }
        });

        fromCamera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                captureImage();
            }
        });
        return rootView;
    }

    /*
     * Capturing Camera Image will lauch camera app requrest image capture
     */
    private void captureImage() {

        StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
        StrictMode.setVmPolicy(builder.build());

        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

        fileUri = getOutputMediaFileUri(MEDIA_TYPE_IMAGE);

        intent.putExtra(MediaStore.EXTRA_OUTPUT, fileUri);

        // start the image capture Intent
        startActivityForResult(intent, CAMERA_CAPTURE_IMAGE_REQUEST_CODE);
    }

    private void getFromGallery(){
        Intent intent = new Intent(
                Intent.ACTION_PICK,
                MediaStore.Images.Media.INTERNAL_CONTENT_URI
        );

        fileUri = getOutputMediaFileUri(MEDIA_TYPE_IMAGE);

        intent.putExtra(MediaStore.EXTRA_OUTPUT, fileUri);

        startActivityForResult(intent, GET_FROM_GALLERY);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        Log.d("samudra","masuk");
        if (requestCode == CAMERA_CAPTURE_IMAGE_REQUEST_CODE) {
            if (resultCode == -1) {
                // successfully captured the image
                // display it in image view
                previewCapturedImage();
                Intent i = new Intent()
                        .putExtra("path","test");
                getTargetFragment().onActivityResult(getTargetRequestCode(),GET_FROM_GALLERY,i);
                getDialog().dismiss();
            }
        } else if(requestCode==GET_FROM_GALLERY){
            if (resultCode==-1){
                Uri selectedImage = data.getData();
                File file = new File(selectedImage.toString());
                if (file.length() > 2048){
                    Toast.makeText(getActivity().getApplicationContext(),"Image size is too large", Toast.LENGTH_SHORT).show();
                    getDialog().dismiss();
                }
                // save to preferences
                String realpath = getRealPathFromUri(getActivity().getApplicationContext(),selectedImage);
                // get current data
                ArrayList<String> photos = new ArrayList<String>();
//                UserPreference.setUploadProduct(getActivity().getApplicationContext(),photos); //  remove all first
                ArrayList<String> allPhoto = Session.getUploadProduct(getActivity().getApplicationContext());
                if (allPhoto!=null){
                    photos = allPhoto;
                }
                photos.add(realpath);

                Session.setUploadProduct(getActivity().getApplicationContext(),photos);

                Intent i = new Intent()
                        .putExtra("path",realpath);

                getTargetFragment().onActivityResult(getTargetRequestCode(),GET_FROM_GALLERY,i);
                getDialog().dismiss();
            }
        }


    }

    /**
     * Display image from a path to ImageView
     */
    private void previewCapturedImage() {
        try {
            // bimatp factory
            BitmapFactory.Options options = new BitmapFactory.Options();

            // downsizing image as it throws OutOfMemory Exception for larger
            // images
            options.inSampleSize = 10;
            String filePath = fileUri.getPath();
            final Bitmap bitmap = BitmapFactory.decodeFile(filePath,
                    options);
            tempUri = getImageUri(getActivity().getApplicationContext(),bitmap);

            // save to preferences
            String realpath = getRealPathFromUri(getActivity().getApplicationContext(),tempUri);
            // get current data
            ArrayList<String> photos = new ArrayList<String>();
//            UserPreference.setUploadProduct(getActivity().getApplicationContext(),photos); //  remove all first
            ArrayList<String> allPhoto = Session.getUploadProduct(getActivity().getApplicationContext());
            if (allPhoto!=null){
                photos = allPhoto;
            }
            photos.add(realpath);

            Session.setUploadProduct(getActivity().getApplicationContext(),photos);

        } catch (NullPointerException e) {
            e.printStackTrace();
        }
    }

    public Uri getImageUri(Context inContext, Bitmap inImage) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        inImage.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
        String path = MediaStore.Images.Media.insertImage(inContext.getContentResolver(), inImage, "photo_temporary", null);
        return Uri.parse(path);
    }

    public static String getRealPathFromUri(Context context, Uri contentUri) {
        Cursor cursor = null;
        try {
            String[] proj = { MediaStore.Images.Media.DATA };
            cursor = context.getContentResolver().query(contentUri, proj, null, null, null);
            int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
            cursor.moveToFirst();
            return cursor.getString(column_index);
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
    }

    private Uri getOutputMediaFileUri(int type) {
        return Uri.fromFile(getOutputMediaFile(type));
    }

    @Nullable
    private static File getOutputMediaFile(int type) {

        // External sdcard location
        File mediaStorageDir = new File(
                Environment
                        .getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES),
                IMAGE_DIRECTORY_NAME);

        // Create the storage directory if it does not exist
        if (!mediaStorageDir.exists()) {
            if (!mediaStorageDir.mkdirs()) {
                Log.d(IMAGE_DIRECTORY_NAME, "Oops! Failed create "
                        + IMAGE_DIRECTORY_NAME + " directory");
                return null;
            }
        }

        // Create a media file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss",
                Locale.getDefault()).format(new Date());
        File mediaFile;
        if (type == MEDIA_TYPE_IMAGE) {
            mediaFile = new File(mediaStorageDir.getPath() + File.separator
                    + "IMG_" + timeStamp + ".jpg");
        } else if (type == MEDIA_TYPE_VIDEO) {
            mediaFile = new File(mediaStorageDir.getPath() + File.separator
                    + "VID_" + timeStamp + ".mp4");
        } else {
            return null;
        }

        return mediaFile;
    }
}
