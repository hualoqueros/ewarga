package ara.e_warga.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.daimajia.slider.library.SliderLayout;
import com.daimajia.slider.library.SliderTypes.BaseSliderView;
import com.daimajia.slider.library.SliderTypes.DefaultSliderView;
import com.daimajia.slider.library.Tricks.ViewPagerEx;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;

import ara.e_warga.R;
import ara.e_warga.session.Session;
import butterknife.BindView;
import butterknife.ButterKnife;

public class FilledUploadFragment extends Fragment implements View.OnClickListener {

    @BindView(R.id.tambah_gambar)
    TextView tambahGambar;
    @BindView(R.id.slider)
    SliderLayout sliderUpload;
    @BindView(R.id.image_1)
    ImageView image1;
    @BindView(R.id.image_2)
    ImageView image2;
    @BindView(R.id.image_3)
    ImageView image3;
    @BindView(R.id.image_4)
    ImageView image4;
    @BindView(R.id.image_5)
    ImageView image5;
    @BindView(R.id.position_number)
    TextView positionNumber;

    private static final int TAMBAH_GAMBAR_REQUEST = 1;

    public FilledUploadFragment()
    {
        // Required empty public constructor
    }

    public static FilledUploadFragment newInstance()
    {
        return new FilledUploadFragment();
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_upload_filled, container, false);
        ButterKnife.bind(this, v);
        return v;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        HashMap<String,String> url_maps = new HashMap<String, String>();
        url_maps.put("Tote Bag Ticket", "https://cdn.evbuc.com/images/34897992/156972202245/1/logo.jpg");
        url_maps.put("Statue", "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcS9n-EhlQ-vKao9mx_H0NLbdzvN28TAGzgQ0Bq0fRwHjIy6sLaBMQ");
        url_maps.put("Wood Chair", "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcShHlJ5NyPd4F890biCnkcT-bdpabDkyxJDKvuptpYP_INp4umn");
        url_maps.put("Kitchen Tools", "https://cdn.evbuc.com/images/39992484/181233124177/1/logo.jpg");

        setupSlider();

        sliderUpload.setPresetTransformer(SliderLayout.Transformer.Default);
        sliderUpload.setPresetIndicator(SliderLayout.PresetIndicators.Center_Bottom);
        sliderUpload.stopAutoCycle();
        sliderUpload.addOnPageChangeListener(new ViewPagerEx.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                positionNumber.setText(""+(position+1));
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        tambahGambar.setOnClickListener(this);
    }

    public void setupSlider(){
        ArrayList<String> listUpload  = Session.getUploadProduct(getActivity().getApplicationContext());
        int index = 1;
        for (String name : listUpload){
            File image = new File(name);
            if (image.exists()) {
                DefaultSliderView textSliderView = new DefaultSliderView(getActivity().getApplicationContext());
                textSliderView
                        .image(image)
                        .setScaleType(BaseSliderView.ScaleType.CenterInside);

                sliderUpload.addSlider(textSliderView);
                renderThumbnail(index,image);
                index++;
            }
        }

    }

    public void renderThumbnail(int index, File image){
        switch (index){
            case 1:
                Picasso.with(getActivity().getApplicationContext()).load(image).into(image1);
                break;
            case 2:
                Picasso.with(getActivity().getApplicationContext()).load(image).into(image2);
                break;
            case 3:
                Picasso.with(getActivity().getApplicationContext()).load(image).into(image3);
                break;
            case 4:
                Picasso.with(getActivity().getApplicationContext()).load(image).into(image4);
                break;
            case 5:
                Picasso.with(getActivity().getApplicationContext()).load(image).into(image5);
                break;
        }
    }


    @Override
    public void onClick(View view) {
        if (view.getId()==R.id.tambah_gambar){
            ArrayList<String> listUpload  = Session.getUploadProduct(getActivity().getApplicationContext());
            if (listUpload.size() < 5) {
                DFragmentPopUpCameraOrGallery popUpGalleryOrCamera = new DFragmentPopUpCameraOrGallery();
                popUpGalleryOrCamera.setTargetFragment(this, TAMBAH_GAMBAR_REQUEST);
                popUpGalleryOrCamera.show(getActivity().getSupportFragmentManager().beginTransaction(), "take_picture_from");
            }else{
                Toast.makeText(getActivity().getApplicationContext(),"Maaf, maksimal 5 gambar",Toast.LENGTH_SHORT).show();
            }
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode==TAMBAH_GAMBAR_REQUEST){
            Bundle bundle = data.getExtras();
            Log.d("filepath",bundle.getString("path",""));

            // reload slider
            FragmentTransaction ftr = getFragmentManager().beginTransaction();
            ftr.detach(FilledUploadFragment.this).attach(FilledUploadFragment.this).commit();

        }
    }

}
