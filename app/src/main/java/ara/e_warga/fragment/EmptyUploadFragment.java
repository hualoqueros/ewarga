package ara.e_warga.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import ara.e_warga.FormActivity;
import ara.e_warga.R;
import butterknife.BindView;
import butterknife.ButterKnife;

public class EmptyUploadFragment extends Fragment implements View.OnClickListener {
    @BindView(R.id.tambah_gambar)
    TextView tambahGambar;

    private static final int TAMBAH_GAMBAR_REQUEST = 1;

    public EmptyUploadFragment()
    {
        // Required empty public constructor
    }

    public static EmptyUploadFragment newInstance()
    {
        return new EmptyUploadFragment();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_layout_empty, container, false);
        ButterKnife.bind(this, v);
        return v;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        tambahGambar.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        if (view.getId()==R.id.tambah_gambar){
            DFragmentPopUpCameraOrGallery popUpGalleryOrCamera = new DFragmentPopUpCameraOrGallery();
            popUpGalleryOrCamera.setTargetFragment(this,TAMBAH_GAMBAR_REQUEST);
            popUpGalleryOrCamera.show(getActivity().getSupportFragmentManager().beginTransaction(),"take_picture_from");
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode==TAMBAH_GAMBAR_REQUEST){
            Bundle bundle = data.getExtras();
            if(bundle!=null){
                if (bundle.getString("path")!=""){
                    // change him self to filled fragment upload
                    ((FormActivity)getActivity()).change(new EmptyUploadFragment());
                }
            }
        }
    }
}
